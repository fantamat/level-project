const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

app.use(express.static(__dirname))

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/assets/index.html'));
});

router.get('/main.css',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/main.css'));
});

router.get('/map.js',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/map.js'));
});

router.get('/films.js',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/films.js'));
});

router.get('/comments.js',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/comments.js'));
});

router.get('/images/landing-image.png',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/images/landing-image.png'));
});

router.get('/images/favicon.ico',function(req,res){
    res.sendFile(path.join(__dirname+'/assets/images/favicon.ico'));
});
  


//add the router
app.use('/', router);
app.listen(process.env.PORT || 8000);

console.log('Running at Port 8000');