var counter = 0;

function addComment() {
    var text = document.getElementById("input-comment").value
    if (text.length == 0) {
        alert("Pleace, write your comment before submition.")
        return
    }
    var date = (new Date()).toISOString()
    console.log(text)
    counter++
    localStorage.setItem(date, text)
    renderComments()
}

function renderComments() {
    $("#comments-number").html(`<p>COMMENTS: ${localStorage.length}</p>`)    
    result = ""
    $.each(localStorage, function(date, text) {
        if (date.endsWith("Z")) {
            result += `
            <div class="col-md-9 container shadow-lg p-5 mb-5 bg-white rounded comment">
                <p class="delay">${humanized_time_span(date)}</p>
                <p class="comment-text">${text}</p>                                                                     
            </div>`
        }
    })
    $("#comments-list").html(result)
}

function handleKeyPressed(e) {
    var keycode = (e.keycode ? e.keycode : e.which)
    if (keycode == "13") {
        addComment()
    }
}
 