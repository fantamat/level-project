function fillFilms() {
    var URL = "https://swapi.co/api/films"
    $.ajax({
        dataType: "json",
        url: URL,
        success: success,
    })
}

function success(e) {
    result = ""
    $.each(e.results, function(index, film) {
        var release_date = new Date(film.release_date)
        result += "<div class='row films-row'>"
        result += `<div class="col-md-1 films-order">${index+1}.</div>`
        result += `<div class="col-md-4 films-name">${film.title}</div>`
        result += `<div class="col-md-4 films-director">${film.director}</div>`
        result += `<div class="col-md-3 films-date">${(release_date.toLocaleDateString("cs")).trim()}</div>`
        result += "</div>"
    })
    result += "<div class='row films-row'><div class='col-md-1 films-order'>...</div></div>"
    $("#films-list-container").html(result)
}