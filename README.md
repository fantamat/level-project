# Star Wars page

Stránka pro fanoušky Star wars live verze je dostupná pod odkazem https://frontend-sw.herokuapp.com/ .

## SASS kompilace

css styly jsou kompilování ze souboru *main.scss*. I když je možné změnit samotný soubor
*assets/main.css*, je místo toho vhodnější měnit soubor *main.scss* jehož kompilací je předešlí soubor přepsán.

Pro kompilaci stačí použít následující příkaz (je nutné mít nainstalovaný sass kompilátor):

`sass main.scss assets/main.css`

## Spuštění dev serveru

Applikace používá *node.js* společně s frameworkem *express*. Při prvním použití je nutné nainstalovat příslušné závislosti, tj. použít příkaz `nmp install`. Poté je možné zpustit server pomocí následující příkazu:

`node app.js`

Stránka je poté dostupná pod odkazem http://localhost:8000/. Při úpravách samotné node.js aplikace je pak lepší použít *nodemon*, který sleduje změny v příslušných souborech a následně restartuje aplikaci.

`nodemon.js`

## Heroku Deployment

Nejprve zkontrolujte zda jste přihlášení a případně se přihlaste `heroku login`.

Při prvním zpuštění aplikace je třeba ji vytvořit pomocí `heroku create appName`(spuštěné aplikace lze zobrazit pomocí `heroku apps`)

Následný update případné zpuštění probíhá pomocí pushe do gitu heroku:

`git push heroku master`
